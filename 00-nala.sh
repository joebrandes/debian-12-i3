#!/bin/bash
# prepare wget for WSL2 Debian 12
sudo apt install wget

# import volian stuff
wget https://gitlab.com/volian/volian-archive/uploads/d00e44faaf2cc8aad526ca520165a0af/volian-archive-nala_0.2.0_all.deb
wget https://gitlab.com/volian/volian-archive/uploads/d9473098bc12525687dc9aca43d50159/volian-archive-keyring_0.2.0_all.deb

# install Nala
sudo apt install ./volian-archive-*.deb
# remove Downloads
rm vol*

# update packages and install nala from volian repos
sudo apt update
# sudo apt show nala
sudo apt install nala -y

# optimize Nala
sudo nala fetch
