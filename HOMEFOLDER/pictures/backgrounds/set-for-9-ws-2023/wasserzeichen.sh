#!/bin/bash
# alle Wasserzeichendateien löschen
rm Wa* 2>/dev/null
# Wasserzeichentext in alle JPEG Bilder aus diesen Verzeichnis einfuegen
# Der Wasserzeichentext wird unten links ins Bild eingebracht
# Sie koennen folgende Parameter anpassen:

Textabstandvonlinks=50
Textabstandvonunten=10
Textabstandvonoben=300
Schriftgroesse=200
PfadFonts="/usr/share/fonts/truetype/msttcorefonts"
# Pfad ist je nach Distribution unterschiedlich!
Schriftart="Arial.ttf"
Schriftfarbe="white"
# Moegliche Farben koennen aufgelistet werden mit dem Befehl: convert -list color
# Wasserzeichentext="Copyright JoeB"
wasserzeichen[0]="Browser"
wasserzeichen[1]="Dateien"
wasserzeichen[2]="Konsolen"
wasserzeichen[3]="Coding"
wasserzeichen[4]="VMS"
wasserzeichen[5]="Office"
wasserzeichen[6]="E-Mail"
wasserzeichen[7]="Graphics"
wasserzeichen[8]="System"

# Programmbeginn
echo "Textabstand von links: $Textabstandvonlinks"
# echo "Textabstand von unten: $Textabstandvonunten"
echo "Textabstand von oben: $Textabstandvonoben"
echo "Schriftgoesse: $Schriftgroesse"
echo "Schriftart: $Schriftart"
echo "Schriftfarbe: $Schriftfarbe"
echo "Wasserzeichentexte: DIVERSE für Workspaces"
# echo "Wasserzeichentext: $Wasserzeichentext"
#echo " "
i=0

for file in *.jpg
do
  horizontal=`identify -verbose $file | grep Geometry: | awk {'print $2'} |cut -d"x" -f 1`
  vertikal=`identify -verbose $file | grep Geometry: | awk {'print $2'} |cut -d"x" -f 2`
  X=$Textabstandvonlinks
  # Y=$(($vertikal - $Textabstandvonunten))
  Y=$Textabstandvonoben

  wasserzeichentext=${wasserzeichen[$i]}

  convert -font $PfadFonts/$Schriftart -pointsize $Schriftgroesse -fill rgba\(255,255,255,0.5\) -draw "text $X, $Y '$wasserzeichentext'" "$file" "`basename Wasserzeichen_"$file"`";
  # convert -font $PfadFonts/$Schriftart -pointsize $Schriftgroesse -fill $Schriftfarbe -draw "text $X, $Y '$Wasserzeichentext'" "$file" "`basename Wasserzeichen_"$file"`";
  # convert -font $PfadFonts/$Schriftart -pointsize $Schriftgroesse -fill rgba\(255,255,255,0.4\)  -gravity center -annotate +0+0 "Joe B." "$file" "`basename Wasserzeichen_"$file"`";
  echo "Bearbeite Datei $file"
  let "i++"
done
echo "Wasserzeichen wurden erfolgreich eingearbeitet"
exit
# Programmende
