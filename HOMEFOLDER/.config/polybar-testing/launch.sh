#!/usr/bin/env bash
# we use different Polybar versions and deplayments
DIR="$HOME/.config/polybar-testing"

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Launch the bar
# The easy way - 1 Monitor and off we go:
#polybar -q main -c "$DIR"/config.ini &

# Launching bars on more than one Monitor: (see for example following reddit link)
# https://www.reddit.com/r/i3wm/comments/mcv5ji/how_to_create_two_polybar_for_dual_monitor/
# =========================================================================================
# Create list/Array of Screens:
screens=$(xrandr --listactivemonitors | grep -v "Monitors" | cut -d" " -f6)
# For Example
# eDP-1 DP-1-1 HDMI-1-1
#screencount=$(xrandr --listactivemonitors | grep -v "Monitors" | cut -d" " -f6 | wc -l)
# For Example: 3

if [[ $(xrandr --listactivemonitors | grep -v "Monitors" | cut -d" " -f4 | cut -d"+" -f2- | uniq | wc -l) == 1 ]]; then
	# we have just one Monitor
	MONITOR=$(polybar --list-monitors | cut -d":" -f1) polybar -q main -c "$DIR/config.ini" &
else
	primary=$(xrandr --query | grep primary | cut -d" " -f1)
	counter=1
	# loop thru the screens:
	for m in $screens; do
		if [[ $primary == $m ]]; then
			MONITOR=$m polybar -q main -c "$DIR/config.ini" &
			# Monitors bejond primary (or single) Monitor need tweaking and more logic
		elif ((counter == 2)); then
			MONITOR=$m polybar -q secondary -c "$DIR/config.ini" &
		else
			MONITOR=$m polybar -q tertiary -c "$DIR/config.ini" &
		fi
		counter=$counter+1
	done
fi
