#!/bin/bash
# Analyse the i3 config script for User Documentation
CONFIGDIR="$(pwd)"
CONFIGFILE="config-BASE"  # later change to config!!!!


print_intro () {
	echo -e "\n================================================="
	echo "BEGIN $1"
	echo "================================================="
}

print_outro () {
	echo -e "\n================================================="
	echo "END $1"
	echo -e "=================================================\n"
}


# grep settings via function:
get_settings () {
	print_intro "Settings"

  grep -B 1 --no-group-separator "^set " $CONFIGDIR/$CONFIGFILE |
      sed 's/# /\nSetting: /g' | sed 's/set //g'

	print_outro "Settings"
}

# grep keybindings via function:
get_keybindings () {
	print_intro "Keybindings"

  grep -B 1 --no-group-separator "^bindsym " $CONFIGDIR/$CONFIGFILE |
      sed 's/# /\nKeybinding: /g' | sed 's/bindsym //g'

	print_outro "Keybindings"
}


# grep autostarts via function:
get_autostarts () {
	print_intro "Autostarts"

  grep -B 1 --no-group-separator "^exec" $CONFIGDIR/$CONFIGFILE |
			grep -v "nogrep$" |
      sed 's/# /\nAutostart: /g'

	print_outro "Autostarts"
}

# grep floating / focus via function:
get_floatfocus () {
print_intro "Floats / Focus"

  grep -B 1 --no-group-separator "^for_window" $CONFIGDIR/$CONFIGFILE |
      sed 's/# /\n/g'

print_outro "Floats / Focus"
}

# grep colors via function:
get_i3colors () {
print_intro "i3 Colors"

  grep -A 2 --no-group-separator "^# Colors:" $CONFIGDIR/$CONFIGFILE |
      sed 's/# Colors/\ni3 Colorvariable/g' | sed 's/set_from_resource //g'

print_outro "i3 Colors"
}

get_16colors () {
	# e.g. https://askubuntu.com/questions/27314/script-to-display-all-terminal-colors

	echo -e "\033[0mForeground Colors 30 to 37"
	echo ""
	echo -e "Normal\t\tBold"
	echo -e "======\t\t===="

	# echo -e "\033[0mNC (No color)"
	echo -e "\033[0;30mBlack\t\t\033[1;30mBLACK (color0)"
	echo -e "\033[0;31mRED\t\t\033[1;31mLIGHT_RED (color1)"
	echo -e "\033[0;32mGREEN\t\t\033[1;32mLIGHT_GREEN (color2)"
	echo -e "\033[0;33mYELLOW\t\t\033[1;33mLIGHT_YELLOW (color3)"
	echo -e "\033[0;34mBLUE\t\t\033[1;34mLIGHT_BLUE (color4)"
	echo -e "\033[0;35mPURPLE\t\t\033[1;35mLIGHT_PURPLE (color5)"
	echo -e "\033[0;36mCYAN\t\t\033[1;36mLIGHT_CYAN (color6)"
	echo -e "\033[0;37mGRAY\t\t\033[1;37mLIGHT_GRAY (color7)"

}



get_colsNeofetch() {

		print_intro "Neofetch Code"

    local blocks blocks2 cols

		block_range=(0 15)
		color_blocks="on"
		block_width=6
		block_height=2
		col_offset="auto"

    if [[ "$color_blocks" == "on" ]]; then
        # Convert the width to space chars.
        printf -v block_width "%${block_width}s"

        # Generate the string.
        for ((block_range[0]; block_range[0]<=block_range[1]; block_range[0]++)); do
            case ${block_range[0]} in
                [0-7])
                    printf -v blocks  '%b\e[3%bm\e[4%bm%b' \
                        "$blocks" "${block_range[0]}" "${block_range[0]}" "$block_width"
                ;;

                *)
                    printf -v blocks2 '%b\e[38;5;%bm\e[48;5;%bm%b' \
                        "$blocks2" "${block_range[0]}" "${block_range[0]}" "$block_width"
                ;;
            esac
        done

        # Convert height into spaces.
        printf -v block_spaces "%${block_height}s"

        # Convert the spaces into rows of blocks.
        [[ "$blocks"  ]] && cols+="${block_spaces// /${blocks}[mnl}"
        [[ "$blocks2" ]] && cols+="${block_spaces// /${blocks2}[mnl}"

        # Add newlines to the string.
        cols=${cols%%nl}
        cols=${cols//nl/
[${text_padding}C${zws}}

        # Add block height to info height.
        ((info_height+=block_range[1]>7?block_height+3:block_height+2))

        case $col_offset in
            "auto") printf '\n\e[%bC%b\n\n' "$text_padding" "${zws}${cols}" ;;
            *) printf '\n\e[%bC%b\n\n' "$col_offset" "${zws}${cols}" ;;
        esac
    fi

    unset -v blocks blocks2 cols

    # Tell info() that we printed manually.
    # prin=1

		print_outro "Neofetch Code"
}


get_settings
get_keybindings
get_autostarts
get_floatfocus
# get_i3colors
get_colsNeofetch
get_16colors
