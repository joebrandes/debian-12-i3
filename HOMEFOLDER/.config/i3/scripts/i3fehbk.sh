#!/bin/env bash
# in i3 config: exec_always --no-startup-id i3fehbk.sh
# https://www.reddit.com/r/i3wm/comments/8ak3ix/different_background_image_per_workspace/
xprop -spy -root _NET_CURRENT_DESKTOP | while read -r event; do
    num=$(i3-msg -t get_workspaces | jq ".[] | select(.focused==true) | .num")
    feh --bg-fill "/home/$USER/pictures/backgrounds/set-for-9-ws/Bild${num}.jpg"
done
