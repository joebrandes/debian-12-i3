#!/bin/bash

# Python VENV
# python3 -m venv $HOME/.venvs/JoebPython
# function only after reboot

python3 -m pip install --upgrade autotiling

# NVM
# curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.7/install.sh | bash 
# function only after reboot

# nvm install --lts


# directly startx after login
# put the following lines in ~/.profile

# start X if we are on tty1, and we just logged in / test -z: is string empty?
# if [ -z "$DISPLAY" ] && [ $(tty) == /dev/tty1 ]; then
#   startx
# fi
