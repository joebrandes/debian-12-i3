**Table of contents - TOC**




# Debian 12 Bookworm with i3 Window Manager

This Repo is a specialised version of [Gitlab Repo JoeBrandes Debian i3 OneDark]
(https://gitlab.com/joebrandes/debian-i3-onedark.git).


```

 ________  _______   ________  ___  ________  ________            _____    _______          ___  ________     
|\   ___ \|\  ___ \ |\   __  \|\  \|\   __  \|\   ___  \         / __  \  /  ___  \        |\  \|\_____  \    
\ \  \_|\ \ \   __/|\ \  \|\ /\ \  \ \  \|\  \ \  \\ \  \       |\/_|\  \/__/|_/  /|       \ \  \|____|\ /_   
 \ \  \ \\ \ \  \_|/_\ \   __  \ \  \ \   __  \ \  \\ \  \      \|/ \ \  \__|//  / /        \ \  \    \|\  \  
  \ \  \_\\ \ \  \_|\ \ \  \|\  \ \  \ \  \ \  \ \  \\ \  \          \ \  \  /  /_/__        \ \  \  __\_\  \ 
   \ \_______\ \_______\ \_______\ \__\ \__\ \__\ \__\\ \__\          \ \__\|\________\       \ \__\|\_______\
    \|_______|\|_______|\|_______|\|__|\|__|\|__|\|__| \|__|           \|__| \|_______|        \|__|\|_______|
                                                                
```

In **Debian i3 OneDark** are more details and infos regarding the
different Software Deployments on my running Machines. So you can find 
more infos on that other Repo!

Remark: from Early 2023 i decided to concentrate on the then upcoming
**Debian 12 Bookworm** distribution. 
So in this repo i concentrate solely on that Linux Version to create an instant
and easy to use **copy & paste** Solution - no more Distro-Hopping for me ;-)
and i also stopped with various **Dotfiles-Managements** (stow, chezmoi,
Bare-Git and Co).

For any other software solutions i use **Docker** and/or **DistroBox**!

**SHORT**

*   Install via Deb12 ISO
*   SW: no DE/WM 
*   Install Git with `sudo apt install git`
*   `mkdir gitlab-projekte && cd gitlab-projekte` 
*   `git clone https://gitlab.com/joebrandes/debian-12-i3`
*   Bashing of 
    *   `bash 00-nala.sh` (includes optimization of Repos)
    *   `bash 10-install-software.sh`
*   Reboot
*   Bashing of 20-install-after-reboot.sh (if you wish...)


# Package Management with Nala

A ``apt show nala`` in Debian 12 shows version 0.12.2 which 
is ab bit old in 2024.

We want improved Debian Package Management with **Nala** 
from the **Volian** Developers.

*   [https://gitlab.com/volian/nala ](https://gitlab.com/volian/nala )

*   [Installation Wiki Page Volian Nala](https://gitlab.com/volian/nala/-/wikis/Installation)

*   [Chris Titus Tech article nala](https://christitus.com/stop-using-apt/)

Version: 0.2.0 at time of writing - please check!


# Install Software and Envirenment

All the software and configurations...

# Stuff after Reboot

...






