#!/bin/bash
# install it ALL - on your own ... - i hope you know what you are doing ;-)
# just ssh in your new Machine and copy & Paste or git clone this Repo before
# rxvt-unicode (instead of rxvt)
# polybar bumblebee-status ueberzug (from main)

# Prepare local folders
mkdir ~/bin
mkdir -p ~/.local/bin
mkdir ~/.fonts
mkdir -p ~/.local/share/fonts
mkdir -p ~/.local/share/applications
mkdir ~/.icons
mkdir ~/tmp
mkdir ~/opt

# cp all the stuff
cp -r ./HOMEFOLDER/. ~

# Update your system
sudo nala update && sudo nala upgrade -y

# Software installs
sudo nala install command-not-found net-tools wakeonlan rofi ranger atool \
    python3-full python3-pip python3-dev python3-pygit2 python3-venv \
    python3-psutil python3-netifaces python3-pygments python3-pil \
    libjpeg-dev zlib1g-dev libxtst-dev libnotify-bin \
    zsh fzf ripgrep bat neofetch lsd cifs-utils \
    highlight htop calc figlet inxi fontforge \
    mc vim git tmux curl wget viewnior ueberzug \
    zathura zathura-pdf-poppler zathura-ps -y


# Neovim
curl -LO https://github.com/neovim/neovim/releases/latest/download/nvim-linux64.tar.gz
sudo rm -rf /opt/nvim-linux64
sudo tar -C /opt -xzf nvim-linux64.tar.gz

# Starship
curl -sS https://starship.rs/install.sh | sh

# NVM
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.7/install.sh | bash 
# function only after reboot
# nvm install --lts

# Python VENV
python3 -m venv $HOME/.venvs/JoebPython
# function only after reboot
# python3 -m pip install --upgrade autotiling


echo ' '
echo '*************'
echo 'Please REBOOT'
echo '*************'