#!/bin/bash
# install it ALL - on your own ... - i hope you know what you are doing ;-)
# just ssh in your new Machine and copy & Paste or git clone this Repo before
# rxvt-unicode (instead of rxvt)
# polybar bumblebee-status ueberzug (from main)

# Prepare local folders
mkdir ~/bin
mkdir -p ~/.local/bin
mkdir ~/.fonts
mkdir -p ~/.local/share/fonts
mkdir -p ~/.local/share/applications
mkdir ~/.icons
mkdir ~/tmp
mkdir ~/opt

# cp all the stuff
cp -r ./HOMEFOLDER/. ~

# Update your system
sudo nala update && sudo nala upgrade -y

# Software installs
sudo nala install xorg xinit xsettingsd xterm aptitude command-not-found \
    i3 i3status i3lock-fancy python3-i3ipc net-tools wakeonlan \
    firefox-esr evince vlc gsimplecal gedit gedit-plugin-text-size thunderbird \
    rxvt-unicode rofi suckless-tools dzen2 picom dunst ranger atool \
    python3-full python3-pip python3-dev python3-pygit2 python3-venv \
    python3-psutil python3-netifaces python3-pygments python3-pil \
    libjpeg-dev zlib1g-dev libxtst-dev libnotify-bin \
    zsh fzf ripgrep fd-find bat neofetch lsd cifs-utils \
    fonts-noto fonts-font-awesome papirus-icon-theme libharfbuzz-bin \
    bumblebee-status polybar arandr nitrogen \
    highlight hsetroot scrot htop calc figlet inxi fontforge \
    thunar nemo lxappearance mc vim git tmux curl wget gconf2 \
    pulseaudio alsa-utils xclip xsel feh viewnior ueberzug \
    yad xdotool xcwd zathura zathura-pdf-poppler zathura-ps -y


# Neovim
curl -LO https://github.com/neovim/neovim/releases/latest/download/nvim-linux64.tar.gz
sudo rm -rf /opt/nvim-linux64
sudo tar -C /opt -xzf nvim-linux64.tar.gz

# Starship
curl -sS https://starship.rs/install.sh | sh

# Kitty
curl -L https://sw.kovidgoyal.net/kitty/installer.sh | sh /dev/stdin

# Create symbolic links to add kitty and kitten to PATH (assuming ~/.local/bin is in
# your system-wide PATH)
ln -sf ~/.local/kitty.app/bin/kitty ~/.local/kitty.app/bin/kitten ~/.local/bin/
# Place the kitty.desktop file somewhere it can be found by the OS
cp ~/.local/kitty.app/share/applications/kitty.desktop ~/.local/share/applications/
# If you want to open text files and images in kitty via your file manager also add the kitty-open.desktop file
cp ~/.local/kitty.app/share/applications/kitty-open.desktop ~/.local/share/applications/
# Update the paths to the kitty and its icon in the kitty.desktop file(s)
sed -i "s|Icon=kitty|Icon=/home/$USER/.local/kitty.app/share/icons/hicolor/256x256/apps/kitty.png|g" ~/.local/share/applications/kitty*.desktop
sed -i "s|Exec=kitty|Exec=/home/$USER/.local/kitty.app/bin/kitty|g" ~/.local/share/applications/kitty*.desktop

# NVM
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.7/install.sh | bash 
# function only after reboot
# nvm install --lts

# Python VENV
python3 -m venv $HOME/.venvs/JoebPython
# function only after reboot
# python3 -m pip install --upgrade autotiling




echo ' '
echo '*************'
echo 'Please REBOOT'
echo '*************'